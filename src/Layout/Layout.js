import React, { Component } from "react";
import { connect } from "react-redux";
import errorHandlerHOC from "../hoc/ErrorHandlerHOC";
import axios from "../axios-orders";
import * as actions from "../store/actions/index";
import Spinner from "../components/UI/Spinner/Spinner";

import Aux from "../hoc/Aux";
// import classes from "./Layout.css";
import Header from "../components/Header/Header";
import OurEmployees from "../containers/OurEmployees";

class Layout extends Component<Props> {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props || nextProps !== this.props;
  }

  componentDidMount = () => {
    this.props.initData();
    this.setState({
      error: true
    });
  };

  shouldComponentUpdate = nextProps => {
    return this.props.employees !== nextProps.employees;
  };

  render() {
    let pageContent;

    if (this.props.employees !== undefined) {
      pageContent = (
        <div>
          <Header {...this.props} />
          <OurEmployees {...this.props} />
        </div>
      );
    } else {
      pageContent = <Spinner />;
    }

    return pageContent;
  }
}

const mapStateToProps = state => {
  return {
    employees: state.ourEmployees.employees,
    error: state.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onShowModal: () => dispatch(actions.showModal()),
    onHideModal: () => dispatch(actions.hideModal()),
    initData: () => dispatch(actions.initData())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(errorHandlerHOC(Layout, axios));
