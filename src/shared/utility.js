export const displayDate = utctime =>
  new Date(utctime).toISOString().substring(0, 10);

export const truncateAtWordBoundary = (str: string, maxLength: number) => {
  return maxLength > str.length
    ? str
    : str.substring(0, str.lastIndexOf(" ", maxLength)) + "...";
};
