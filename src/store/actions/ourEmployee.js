import * as actionTypes from "./actionTypes";
import axios from "../../axios-orders";

export const hideModal = () => {
  return {
    type: actionTypes.HIDE_MODAL
  };
};

export const showModal = () => {
  return {
    type: actionTypes.SHOW_MODAL
  };
};

export const setCompanyInfo = companyInfo => {
  return {
    type: actionTypes.SET_COMPANYINFO,
    companyInfo: { ...companyInfo }
  };
};

export const setEmployees = employees => {
  return {
    type: actionTypes.SET_EMPLOYEES,
    employees: [...employees]
  };
};

export const fetchDateFailed = () => {
  return {
    type: actionTypes.FETCH_DATA_FAILED
  };
};

export const initData = () => {
  return dispatch => {
    axios
      .get("https://nate-277a7.firebaseio.com/.json")
      .then(response => {
        dispatch(setCompanyInfo(response.data.companyInfo));
        dispatch(setEmployees(response.data.employees));
      })
      .catch(error => {
        dispatch(fetchDateFailed());
      });
  };
};
