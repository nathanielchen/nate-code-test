export {
  showModal,
  hideModal,
  initData,
  companyInfo,
  employees
} from "./ourEmployee";
