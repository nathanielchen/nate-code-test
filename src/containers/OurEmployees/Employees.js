import React, { Component } from "react";
import { connect } from "react-redux";
import errorHandlerHOC from "../../hoc/ErrorHandlerHOC";
import axios from "../../axios-orders";
import Employee from "./Employee/Employee";
import "./Employees.css";

class Employees extends Component<Props> {
  shouldComponentUpdate = nextProps => {
    return this.props.employees !== nextProps.employees;
  };

  render() {
    let employeesList = null;
    if (this.props.employees !== null) {
      employeesList = this.props.employees.map(employee => (
        <Employee
          viewingModalHandler={this.props.viewingModalHandler}
          key={employee.id}
          employee={employee}
          {...employee}
        />
      ));
    }

    return <div className="employee_list row">{employeesList}</div>;
  }
}

const mapStateToProps = state => {
  return {
    employees: state.ourEmployees.employees
  };
};

export default connect(mapStateToProps)(errorHandlerHOC(Employees, axios));
