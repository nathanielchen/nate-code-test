---
 __        __       _                       ____          _        _____        _   
 \ \      / ___ ___| |_ _ __   __ _  ___   / ___|___   __| | ___  |_   ____ ___| |_ 
  \ \ /\ / / _ / __| __| '_ \ / _` |/ __| | |   / _ \ / _` |/ _ \   | |/ _ / __| __|
   \ V  V |  __\__ | |_| |_) | (_| | (__  | |__| (_) | (_| |  __/   | |  __\__ | |_ 
    \_/\_/ \___|___/\__| .__/ \__,_|\___|  \____\___/ \__,_|\___|   |_|\___|___/\__|
                       |_|
---

# Westpac Code Test

This app started from [`WDP UI code test`](https://github.com/facebookincubator/create-react-app).

# Quickstart

Install [NVM](https://github.com/creationix/nvm) and then run,

    nvm install

Then,

    yarn install

    yarn build

    yarn start

## Helping diagram images
Please find the system structure diagram for the overview of the system.

CodeText application system structure diagram.jpg
hand drawn system structure-UML.jpg


## This code requires a internet connection to operate

    API to Firebase
    https://nate-277a7.firebaseio.com/.json

    Image accest from S3
    https://s3.amazonaws.com/uifaces/faces/twitter/derekebradley/

## Style (CSS)

## The Application Requirements

This simple employee roster app will use a static sample-data.json file with randomly generated sample data that should be used to create a simple one-page app of the roster for the company's employees.

The roster should be represented in a "card" layout (see wireframe grid-view.png) which initially shows minimal employee information consisting of the employee's name, avatar picture and a truncated excerpt of their bio.

Clicking on a card should highlight the card in some way and render additional information in a modal overlay (see wireframe detail-view.png). Clicking the close (X) icon above the modal should close the modal and remove the highlight from the card that was clicked. Clicking anywhere outside the modal's content should also close the modal.

Your application should render correctly in a mobile sized device.

Note: If anything is unclear, please create a readme.md file in the /code directory to document the assumptions you've made.
